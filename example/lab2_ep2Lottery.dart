import 'dart:io';

void main() {
  String? round;
  while (round != "exit") {
    var Num = (stdin.readLineSync()!);
    print(check(Num));
    round = Num;
  }
}

String check(String x) {
  if (check1s(x) != "") {
    return check1s(x);
  } else if (check6s(x) != "") {
    return check6s(x);
  } else if (check2s(x) != "") {
    return check2s(x);
  } else if (check3s(x) != "") {
    return check3s(x);
  } else if (check4s(x) != "") {
    return check4s(x);
  } else if (check5s(x) != "") {
    return check5s(x);
  } else if (checkPre(x) != "") {
    return checkPre(x);
  } else if (checkPos(x) != "") {
    return checkPos(x);
  } else if (checkPos2(x) != "") {
    return checkPos2(x);
  }
  return "เสียใจด้วย คุณไม่ถูกรางวัล";
}

List<String> rew1p() {
  var rew1 = ['436594'];
  return rew1;
}

List<String> rew2p() {
  var rew2 = ['502412', '285563', '396501', '084971', '049364'];
  return rew2;
}

List<String> rew3p() {
  var rew3 = [
    '996939',
    '043691',
    '422058',
    '853019',
    '662884',
    '166270',
    '666926',
    '896753',
    '242496',
    '575619'
  ];
  return rew3;
}

List<String> rew4p() {
  var rew4 = [
    '345541',
    '675634',
    '240354',
    '124121',
    '263978',
    '293437',
    '098790',
    '011052',
    '550340',
    '400877'
  ];
  return rew4;
}

List<String> rew5p() {
  var rew5 = [
    '033687',
    '872012',
    '321889',
    '124285',
    '116886',
    '697291',
    '545548',
    '110635',
    '923777',
    '234349',
    '007464',
    '296893'
  ];
  return rew5;
}

List<String> pre3rew() {
  var rew = ['893', '266'];
  return rew;
}

List<String> pos3rew() {
  var rew = ['447', '282'];
  return rew;
}

List<String> pos2rew() {
  var rew = ['14'];
  return rew;
}

List<String> rew6() {
  var rew = ['436593', '436595'];
  return rew;
}

String check1s(String x) {
  if (rew1p().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลที่ 1 ได้รับเงินรางวัลมูล่า 6000000 บาท";
  return "";
}

String check2s(String x) {
  if (rew2p().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลที่ 2 ได้รับเงินรางวัลมูล่า 200000 บาท";
  return "";
}

String check3s(String x) {
  if (rew3p().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลที่ 3 ได้รับเงินรางวัลมูล่า 80000 บาท";
  return "";
}

String check4s(String x) {
  if (rew4p().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลที่ 4 ได้รับเงินรางวัลมูล่า 40000 บาท";
  return "";
}

String check5s(String x) {
  if (rew5p().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลที่ 5 ได้รับเงินรางวัลมูล่า 20000 บาท";
  return "";
}

String check6s(String x) {
  if (rew6().contains(x))
    return "ยินดีด้วย! คุณถูกรางวัลข้างเคียง ได้รับเงินรางวัลมูล่า 100000 บาท";
  return "";
}

String checkPre(String x) {
  String find = x.substring(0, 3);
  if (pre3rew().contains(find))
    return "ยินดีด้วย! คุณถูกรางวัลเลข 3 ตัวหน้า ได้รับเงินรางวัลมูล่า 4000 บาท";
  return "";
}

String checkPos(String x) {
  String find = x.substring(3);
  if (pos3rew().contains(find))
    return "ยินดีด้วย! คุณถูกรางวัลเลข 3 ตัวท้าย ได้รับเงินรางวัลมูล่า 4000 บาท";
  return "";
}

String checkPos2(String x) {
  String find = x.substring(4, 6);
  if (pos2rew().contains(find))
    return "ยินดีด้วย! คุณถูกรางวัลเลข 2 ตัวหน้า ได้รับเงินรางวัลมูล่า 4000 บาท";
  return "";
}
