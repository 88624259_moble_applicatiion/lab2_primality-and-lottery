import 'dart:io' show stdin;

void main() {
  int? round;
  while (round != 0) {
    //หยุดเมื่อใส่ 0
    int input = int.parse(stdin.readLineSync()!);
    print(primality(input));
    round = input;
  }
}

bool primality(int x) {
  int k = 2;
  while (true) {
    if (k >= x) return true; // หลุดจากวงวนเมื่อลองหารครบทุกตัวแล้ว
    if ((x % k) == 0) return false; // หลุดจากวงวนเมื่อพบ k ที่หาร n ลงตัว
    k++;
  }
}
